#!perl

use strict;
use warnings;
use utf8;

return {
    site_title          => 'HostingSpec.net',
    site_description    => 'レンタルサーバ丸かじり！　レンタルサーバレビュー、サイト運営Tipsサイト',
    site_language       => 'ja',
    site_url            => 'http://hostingspec.net',

    category_label      => {
        'reviews'           => 'レンタルサーバレビュー',
        'reviews/shared'    => '共有サーバ',
        'reviews/vps'       => 'VPS',
        'reviews/dedicated' => '専用サーバ',
        
        'tips'              => 'サイト運営Tips',
    },
};
