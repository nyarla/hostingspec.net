# basic settings
data_dir:       ./entries
templates_dir:  ./templates
site_dir:       ./output

# timezone
time_zone: Asia/Tokyo

# plugins
plugins:
  # template
  - module: Template::MicroTemplate
  
  # entry
  - module: PlasxomMeta
  - module: MultiMarkdown
    config:
      markdown_args:
        use_metadata: 0
        base_header_level: 3
  - module: Entry::File

  # .htaccess
  - module: StaticFile
    config:
      regex: ^.htaccess
  
  # static files
  - module: StaticFile
  
  # permalink
  - module: Render::Entry
  
  # reviews index
  - module: AutoIndex
    config:
      fitler: $entry->path =~ m{^/reviews};
      path:   /reviews
      filename: index.html
  
  # feed
  - module: AutoIndex
    config:
      path: /
      filename: index.atom
  
  # sitemap.xml
  - module: AutoIndex
    config:
      path: /
      filename: sitemap.xml
